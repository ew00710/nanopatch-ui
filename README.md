# NanoPatch

Run deployment bash script (if `dist` folder is not present) \
`./deploy.sh`

Install required packages \
`pip install -r requirements.txt`

Run NanoPatch UI Locally \
`flask run`

Top make Flask server visible to network \
`flask run --host=0.0.0.0`

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
